FROM alpine:3.8
RUN mkdir -p /go/src/device-data-receiver
ADD ./build/main /go/src/device-data-receiver
ADD ./index.html /go/src/device-data-receiver
ADD ./report.html /go/src/device-data-receiver
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
WORKDIR /go/src/device-data-receiver
CMD ["/go/src/device-data-receiver/main"]
