.PHONY: deps vet test dev build clean

DOCKER_REPO_URL = yenchieh/websocket-recevier

deps:
	go mod download && go mod vendor

build: clean
	GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -o ./build/main *.go

dev:
	go run main.go

clean:
	rm -rf build/*
	find . -name '*.test' -delete

push-image: build build-image
	docker push $(DOCKER_REPO_URL):latest

build-image:
	docker build --rm -t $(DOCKER_REPO_URL):latest .