# [View Data Report Page](http://167.99.171.123:8080/report)

## - GET /api/report
- ContentType: ```application/json```
- Parameters:

| Key | Type | Example |
|-----|------|---------|
|id|String|AB001|
|dateFrom|String|2020-05-01|
|dateTo|String|2020-05-30|
|battery1From|Integer|5|
|battery1To|Integer|100|
|battery2From|Integer|5|
|battery2To|Integer|100|

- Response JSON:
```
{
    "error": "error reason", # Only when there has an error
    "data": [
        {
            "id": String,
            "gps": {
                "lat": String,
                "long": String
            },
            "battery": {
                "battery1": Integer,
                "battery2": Integer
            },
            "motion": {
                "x": String,
                "y": String,
                "z": String
            },
            "date": String,         # Format: YYYY-MM-DDThh:mm:ss
            "date_uploaded": String # Format: YYYY-MM-DDThh:mm:ss
        }
    ]
}
```

- Example Response:
```
curl --request GET 'localhost:8080/api/report?dateFrom=2020-05-01&dateTo=2020-05-30&battery1From=5&battery1To=100'

# Response:
{
    "data": [
        {
            "id": "AB002",
            "gps": {
                "lat": "121.3054019",
                "long": "25.0338953"
            },
            "battery": {
                "battery1": 40,
                "battery2": 100
            },
            "motion": {
                "x": "12",
                "y": "3",
                "z": "52"
            },
            "date": "2020-05-29T15:12:11",
            "date_uploaded": "2020-05-31T10:02:46"
        },
        {
            "id": "AB001",
            "gps": {
                "lat": "121.3051712",
                "long": "25.0336875"
            },
            "battery": {
                "battery1": 70,
                "battery2": 5
            },
            "motion": {
                "x": "286.00",
                "y": "-278.00",
                "z": "993.00"
            },
            "date": "2020-05-25T09:15:42",
            "date_uploaded": "2020-05-26T07:02:47"
        }
    ]
}
```

## - Get /download
* This API is to download CSV file
* The parameters are the same as ```/api/report```
