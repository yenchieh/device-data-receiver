package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

type GPS struct {
	Lat  string `bson:"lat" json:"lat" csv:"latitude"`
	Long string `bson:"long" json:"long" csv:"longitude"`
}

type Battery struct {
	Battery1 int64 `bson:"battery1" json:"battery1" csv:"battery1"`
	Battery2 int64 `bson:"battery2" json:"battery2" csv:"battery2"`
}

type Motion struct {
	X string `bson:"x" json:"x" csv:"x"`
	Y string `bson:"y" json:"y" csv:"y"`
	Z string `bson:"z" json:"z" csv:"z"`
}

type DeviceData struct {
	ID                   string    `bson:"device_id" json:"id" csv:"id"`
	GPS                  GPS       `bson:"gps" json:"gps" csv:"-"`
	Battery              Battery   `bson:"battery" json:"battery" csv:"-"`
	Motion               Motion    `bson:"motion" json:"motion" csv:"-"`
	Date                 time.Time `bson:"date" json:"-" csv:"-"`
	DateResponse         string    `bson:"-" json:"date" csv:"date"`
	DateUploaded         time.Time `bson:"date_uploaded" json:"-" csv:"-"`
	DateUploadedResponse string    `bson:"-" json:"date_uploaded" csv:"date_uploaded"`
}

type DeviceDataImport struct {
	ID                   string    `csv:"id"`
	Lat string `csv:"lat"`
	Long string `csv:"long"`
	Battery1 int64 `csv:"battery1"`
	Battery2 int64 `csv:"battery2"`
	X string `csv:"x"`
	Y string `csv:"y"`
	Z string `csv:"z"`
	Date                 string `csv:"date"`
}

type Filter struct {
	ID              string
	Battery1From    int64
	Battery1To      int64
	Battery2From    int64
	Battery2To      int64
	FilterDate      bool
	DateFromRequest string
	DateToRequest   string
	DateFrom        time.Time
	DateTo          time.Time
}

const MongoStoreKey = "MONGOSTORE"

func (m *MongoDatastore) InsertData(ctx context.Context, deviceData *DeviceData) error {
	c := m.DB.Collection(DeviceDataDBKey)
	if _, err := c.InsertOne(ctx, deviceData); err != nil {
		return err
	}
	return nil
}

func (m *MongoDatastore) RetrieveData(ctx context.Context, filter *Filter) ([]*DeviceData, error) {

	f := []bson.D{}
	if filter.ID != "" {
		f = append(f, bson.D{{
			"device_id", filter.ID,
		}})
	}
	if filter.Battery1From != 0 && filter.Battery1To != 0 {
		f = append(f, bson.D{{
			"battery.battery1", bson.M{
				"$gte": filter.Battery1From,
				"$lte": filter.Battery1To,
			},
		}})
	}
	if filter.Battery2From != 0 && filter.Battery2To != 0 {
		f = append(f, bson.D{{
			"battery.battery2", bson.M{
				"$gte": filter.Battery2From,
				"$lte": filter.Battery2To,
			},
		}})
	}
	if filter.FilterDate {
		f = append(f, bson.D{{
			"date", bson.M{
				"$gt": filter.DateFrom,
				"$lt": filter.DateTo,
			},
		}})
	}
	finalF := bson.D{}
	if len(f) > 0 {
		finalF = bson.D{{"$and", f}}
	}
	c := m.DB.Collection(DeviceDataDBKey)
	cur, err := c.Find(ctx, finalF)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	deviceDatas := []*DeviceData{}
	for cur.Next(ctx) {
		var deviceData DeviceData
		if err := cur.Decode(&deviceData); err != nil {
			return nil, err
		}
		deviceData.DateResponse = deviceData.Date.Format("2006-01-02T15:04:05")

		deviceData.DateUploadedResponse = deviceData.DateUploaded.Format("2006-01-02T15:04:05")
		deviceDatas = append(deviceDatas, &deviceData)
	}
	return deviceDatas, nil
}
