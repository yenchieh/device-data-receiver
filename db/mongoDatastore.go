package db

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo/readpref"

	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	DeviceDataDBKey = "deviceData"
)

type MongoConfig struct {
	User     string
	Password string
	Host     string
	Database string
}

type MongoDatastore struct {
	DB      *mongo.Database
	Session *mongo.Client
}

func NewDatastore(config MongoConfig) *MongoDatastore {
	var mongoDatastore *MongoDatastore
	db, session := connect(config)
	if db != nil && session != nil {

		if err := session.Ping(context.Background(), readpref.Primary()); err != nil {
			log.Fatal(err)
		}

		mongoDatastore = new(MongoDatastore)
		mongoDatastore.DB = db
		mongoDatastore.Session = session
		return mongoDatastore
	}
	log.Fatal("Failed to connect to database")
	return nil
}

func connect(config MongoConfig) (d *mongo.Database, c *mongo.Client) {
	var connectOnce sync.Once
	var db *mongo.Database
	var session *mongo.Client
	connectOnce.Do(func() {
		db, session = connectToMongo(config)
	})

	return db, session
}

func connectToMongo(config MongoConfig) (d *mongo.Database, c *mongo.Client) {
	uri := fmt.Sprintf(`mongodb://%s:%s@%s/%s`,
		config.User,
		config.Password,
		config.Host,
		config.Database,
	)
	var err error
	session, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = session.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	var DB = session.Database(config.Database)
	log.Printf("CONNECTED: %s\n", config.Database)

	return DB, session
}
