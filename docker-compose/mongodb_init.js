db.createUser({
  user: 'jay',
  pwd: 'jay-pass',
  roles: [
    {
      role: 'readWrite',
      db: 'deviceData'
    }
  ]
})

db.deviceData.createIndex(
  {
    mac_address: 1
  }
)
