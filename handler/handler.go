package handler

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	"websocket-receiver/db"

	"github.com/gocarina/gocsv"

	"github.com/gin-gonic/gin"
)

func ReportPage(c *gin.Context) {
	ctx, cancel := context.WithTimeout(c, 10*time.Second)
	defer cancel()
	mongoDatastore := c.MustGet(db.MongoStoreKey).(*db.MongoDatastore)

	filter := GetFilterFromRequest(c)
	deviceDatas, err := mongoDatastore.RetrieveData(ctx, filter)
	if err != nil {
		log.Printf("Error on retrieve data. %+v", err)
	}
	for i := len(deviceDatas)/2 - 1; i >= 0; i-- {
		opp := len(deviceDatas) - 1 - i
		deviceDatas[i], deviceDatas[opp] = deviceDatas[opp], deviceDatas[i]
	}
	log.Printf("%d", len(deviceDatas))

	c.HTML(http.StatusOK, "report.html", gin.H{
		"DeviceDatas": deviceDatas,
		"Filter":      filter,
	})
}

func ReportJSON(c *gin.Context) {
	ctx, cancel := context.WithTimeout(c, 10*time.Second)
	defer cancel()
	mongoDatastore := c.MustGet(db.MongoStoreKey).(*db.MongoDatastore)

	filter := GetFilterFromRequest(c)
	deviceDatas, err := mongoDatastore.RetrieveData(ctx, filter)
	if err != nil {
		log.Printf("Error on retrieve data. %+v", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"data":  []string{},
			"error": err.Error(),
		})
		return
	}
	for i := len(deviceDatas)/2 - 1; i >= 0; i-- {
		opp := len(deviceDatas) - 1 - i
		deviceDatas[i], deviceDatas[opp] = deviceDatas[opp], deviceDatas[i]
	}

	c.JSON(http.StatusOK, gin.H{
		"data": deviceDatas,
	})
}

func Download(c *gin.Context) {
	ctx, cancel := context.WithTimeout(c, 10*time.Second)
	defer cancel()
	mongoDatastore := c.MustGet(db.MongoStoreKey).(*db.MongoDatastore)

	filter := GetFilterFromRequest(c)
	deviceDatas, err := mongoDatastore.RetrieveData(ctx, filter)
	if err != nil {
		log.Printf("Error on retrieve data. %+v", err)
	}
	for i := len(deviceDatas)/2 - 1; i >= 0; i-- {
		opp := len(deviceDatas) - 1 - i
		deviceDatas[i], deviceDatas[opp] = deviceDatas[opp], deviceDatas[i]
	}

	csvContent, err := gocsv.MarshalString(&deviceDatas)
	if err != nil {
		panic(err)
	}
	c.Writer.Header().Set("Content-Disposition", "attachment; filename=report.csv")
	c.Writer.Header().Set("Content-type", c.Request.Header.Get("Content-Type"))
	c.Writer.Header().Set("Content-Length", strconv.Itoa(len(csvContent)))
	c.Writer.WriteString(csvContent)
}

func ImportCSV(c *gin.Context) {
	multiFile, err := c.FormFile("file")
	if err != nil {
		log.Printf("Error on getting file")
		c.JSON(http.StatusBadRequest, err)
		return
	}
	file, err := multiFile.Open()
	if err != nil {
		log.Printf("Error on decode file")
		c.JSON(http.StatusBadRequest, err)
		return
	}
	defer file.Close()
	b, err := ioutil.ReadAll(file)
	deviceData := []*db.DeviceDataImport{}
	if err := gocsv.UnmarshalBytes(b, &deviceData); err != nil {
		log.Printf("Error on unmarsh bytes. %#v", err)
		c.JSON(http.StatusBadRequest, err)
		return
	}

	mongoDatastore := c.MustGet(db.MongoStoreKey).(*db.MongoDatastore)
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()

	for _, data := range deviceData{

		dd, err := time.Parse("2006-01-02 15:04:05", data.Date)
		if err != nil {
			log.Printf("Err on parse date. %+v", err)
			return
		}
		d := &db.DeviceData{
			ID:                   data.ID,
			GPS:                  db.GPS{
				Lat: data.Lat,
				Long: data.Long,
			},
			Battery:              db.Battery{
				Battery1: data.Battery1,
				Battery2: data.Battery2,
			},
			Motion:               db.Motion{
				X: data.X,
				Y: data.Y,
				Z: data.Z,
			},
			Date:                 dd,
			DateUploaded:         time.Now(),
		}
		if err := mongoDatastore.InsertData(ctx, d); err != nil {
			log.Printf("Error on inserting data. %+v", err)
		}

	}
	c.JSON(http.StatusOK, gin.H{})

}

func StringToInt64(s string) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return 0
	}
	return i
}

// Format: yyyy-mm-dd
func StringToDate(s string) time.Time {
	ss := strings.Split(s, "-")
	year := StringToInt64(ss[0])
	month := StringToInt64(ss[1])
	day := StringToInt64(ss[2])
	return time.Date(int(year), time.Month(int(month)), int(day), 0, 0, 0, 0, time.UTC)
}

func GetFilterFromRequest(c *gin.Context) *db.Filter {
	battery1To := c.Query("battery1To")
	battery1From := c.Query("battery1From")
	battery2To := c.Query("battery2To")
	battery2From := c.Query("battery2From")
	filter := &db.Filter{
		ID:              c.Query("id"),
		Battery1From:    StringToInt64(battery1From),
		Battery1To:      StringToInt64(battery1To),
		Battery2From:    StringToInt64(battery2From),
		Battery2To:      StringToInt64(battery2To),
		DateFromRequest: c.Query("dateFrom"),
		DateToRequest:   c.Query("dateTo"),
	}
	if len(filter.DateFromRequest) > 4 && len(filter.DateToRequest) > 4 {
		filter.DateFrom = StringToDate(filter.DateFromRequest)
		filter.DateTo = StringToDate(filter.DateToRequest)
		filter.FilterDate = true
	}
	return filter
}

func CalculateLocation(s string) string {
	latArray := strings.Split(s, ".")
	latString := fmt.Sprintf("%s.%s", latArray[1][:2], latArray[1][2:])
	lat2, err := strconv.ParseFloat(latString, 10)
	if err != nil {
		log.Printf("Error on parse to float: %+v", err)
		return ""
	}
	lat1, err := strconv.ParseFloat(latArray[0], 10)
	if err != nil {
		log.Printf("Error on parse to int: %+v", err)
		return ""
	}

	return fmt.Sprintf("%.7f", (lat2/60)+lat1)
}

func DateToString(t time.Time) string {
	return fmt.Sprintf("%d-%d-%d %d:%d:%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
}
