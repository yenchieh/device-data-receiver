package handler

import (
	"websocket-receiver/db"

	"github.com/gin-gonic/gin"
)

func Middleware(mongo *db.MongoDatastore) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(db.MongoStoreKey, mongo)
		c.Next()
	}
}
