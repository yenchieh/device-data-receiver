package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
	"websocket-receiver/db"
	"websocket-receiver/handler"

	"github.com/urfave/cli"

	"github.com/gin-gonic/gin"

	"golang.org/x/net/websocket"
)

func main() {

	app := cli.NewApp()
	app.Name = "device-data-receiver"

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:   "debug",
			EnvVar: "DEBUG",
		},
		cli.StringFlag{
			EnvVar: "MONGODB_USER",
			Name:   "mongodb_user",
			Value:  "jay",
		},
		cli.StringFlag{
			EnvVar: "MONGODB_PASSWORD",
			Name:   "mongodb_password",
			Value:  "jay-pass",
		},
		cli.StringFlag{
			EnvVar: "MONGODB_DATABASE",
			Name:   "mongodb_database",
			Value:  "deviceData",
		},
		cli.StringFlag{
			EnvVar: "MONGODB_HOST",
			Name:   "mongodb_host",
			Value:  "localhost:27017",
		},
		cli.StringFlag{
			EnvVar: "WEB_PORT",
			Name:   "web_port",
			Value:  ":8080",
		},
		cli.StringFlag{
			EnvVar: "TCP_PORT",
			Name:   "tcp_port",
			Value:  ":9001",
		},
	}

	app.Action = func(c *cli.Context) error {

		mongoConfig := db.MongoConfig{
			User:     c.String("mongodb_user"),
			Password: c.String("mongodb_password"),
			Host:     c.String("mongodb_host"),
			Database: c.String("mongodb_database"),
		}
		mongoDatastore := db.NewDatastore(mongoConfig)

		router := gin.Default()
		router.SetFuncMap(template.FuncMap{
			"location": handler.CalculateLocation,
			"dateToString": handler.DateToString,
		})

		router.LoadHTMLFiles("index.html", "report.html")
		router.GET("/", func(c *gin.Context) {
			var revertLogs []*Logs
			for i := len(logs.Logs) - 1; i >= 0; i-- {
				revertLogs = append(revertLogs, logs.Logs[i])
			}
			c.HTML(http.StatusOK, "index.html", gin.H{
				"Logs": revertLogs,
			})
		})

		router.POST("/data", func(c *gin.Context) {
			data, err := c.GetRawData()
			if err != nil {
				logs.AddError(fmt.Sprintf("Receiving data error: %+v", err))
				return
			}
			logs.AddPrimaryLog(fmt.Sprintf("[HTTP] Received Data (%s): %s", c.ClientIP(), string(data)))
		})

		router.GET("/report", handler.Middleware(mongoDatastore), handler.ReportPage)
		router.GET("/download", handler.Middleware(mongoDatastore), handler.Download)
		router.POST("/import", handler.Middleware(mongoDatastore), handler.ImportCSV)

		api := router.Group("/api")
		api.GET("/report", handler.Middleware(mongoDatastore), handler.ReportJSON)

		go func() {
			router.Run(c.String("web_port"))
		}()

		l, err := net.Listen("tcp", c.String("tcp_port"))
		if err != nil {
			return err
		}
		defer l.Close()
		rand.Seed(time.Now().Unix())

		for {
			c, err := l.Accept()
			if err != nil {
				log.Fatal(err)
			}
			logs.AddLog(fmt.Sprintf("[TCP] New connection from: %s", c.RemoteAddr().String()))

			go TCPHandler(context.Background(), c, mongoDatastore)
		}
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}

}

func TCPHandler(ctx context.Context, conn net.Conn, mongoDatastore *db.MongoDatastore) {
	for {
		netData, err := bufio.NewReader(conn).ReadString('\n')
		logs.AddPrimaryLog(fmt.Sprintf("[TCP] Received Data (%s): %s", conn.RemoteAddr().String(), netData))
		if err != nil {
			fmt.Printf("Error receiving data: %+v", err)
			logs.AddError(err.Error())
			return
		}
		if strings.Contains(netData, "STOP") {
			fmt.Printf("Existing TCP server")
			logs.AddLog("Disconnected TCP connection")
			conn.Close()
			return
		}
		arr := strings.Split(netData, "@")
		if len(arr) != 9 {
			logs.AddError(fmt.Sprintf("The received data format was not correct: %s", netData))
			return
		}
		// Save data
		if err := HandleDataStore(ctx, arr, mongoDatastore); err != nil {
			logs.AddError(fmt.Sprintf("Error on store data to database. %+v", err))
		}
	}
}

func HandleDataStore(ctx context.Context, data []string, mongoDatastore *db.MongoDatastore) error {
	// 2503.3895300N@12130.540190E@40@100@12@3@52@AB002@290520151211 經度@緯度@電量1@電量2@x@y@z@id@date
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	Long := data[0][:len(data[0])-1]
	longN := data[0][len(data[0])-1:]
	if longN != "N" {
		Long = "-" + Long
	}

	// Process location
	Lat := data[1][:len(data[1])-1]
	LatN := data[1][len(data[1])-1:]
	if LatN != "E" {
		Lat = "-" + Lat
	}
	LatF, err := strconv.ParseFloat(Lat, 64)
	if err != nil {
		return err
	}
	LatF = LatF / 100

	LongF, err := strconv.ParseFloat(Long, 64)
	if err != nil {
		return err
	}
	LongF = LongF / 100

	battery1 := handler.StringToInt64(data[2])
	battery2 := handler.StringToInt64(data[3])

	// Convert date ddmmyyhhmmss
	dateString := data[8]
	if len(dateString) < 12 {
		return errors.New("error on date format")
	}
	day := handler.StringToInt64(dateString[0:2])
	month := handler.StringToInt64(dateString[2:4])
	year := handler.StringToInt64("20" + dateString[4:6])
	hour := handler.StringToInt64(dateString[6:8])
	minute := handler.StringToInt64(dateString[8:10])
	second := handler.StringToInt64(dateString[10:12])
	t := time.Date(int(year), time.Month(int(month)), int(day), int(hour), int(minute), int(second), 0, time.UTC)

	if err := mongoDatastore.InsertData(ctx, &db.DeviceData{
		GPS: db.GPS{
			Long: fmt.Sprintf("%.7f", LongF),
			Lat:  fmt.Sprintf("%.7f", LatF),
		},
		Battery: db.Battery{
			Battery1: battery1,
			Battery2: battery2,
		},
		Motion: db.Motion{
			X: data[4],
			Y: data[5],
			Z: data[6],
		},
		ID:           data[7],
		Date:         t,
		DateUploaded: time.Now(),
	}); err != nil {
		return err
	}

	return nil
}

type Client struct {
	ID string `json:"id"`
}

var connectionNum = 0
var clients = make(map[*websocket.Conn]*Client)

type ClientLogs struct {
	Logs []*Logs
}

type Logs struct {
	Type    string
	Content string
}

func (l *ClientLogs) AddError(log string) {
	date := time.Now()
	l.Logs = append(l.Logs, &Logs{
		Type:    "error",
		Content: fmt.Sprintf("%d/%d/%d %d:%d:%d | %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), log),
	})
}

func (l *ClientLogs) AddLog(log string) {
	date := time.Now()
	l.Logs = append(l.Logs, &Logs{
		Type:    "info",
		Content: fmt.Sprintf("%d/%d/%d %d:%d:%d | %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), log),
	})
}

func (l *ClientLogs) AddPrimaryLog(log string) {
	date := time.Now()
	l.Logs = append(l.Logs, &Logs{
		Type:    "primary",
		Content: fmt.Sprintf("%d/%d/%d %d:%d:%d | %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), log),
	})
}

var logs ClientLogs

func connect(connection *websocket.Conn) {
	connectionNum++
	client := Client{
		ID: fmt.Sprintf("%d", connectionNum),
	}
	clients[connection] = &client
	logs.AddLog(fmt.Sprintf("Client Added ID: %s", clients[connection].ID))

	fmt.Println("Client Added")

	for {
		var receiveMessage string

		if err := websocket.Message.Receive(connection, &receiveMessage); err != nil {
			log.Printf("Error on receiving: %+v", err)
			logs.AddError(fmt.Sprintf("Error on receiving message: %+v", err))
			logs.AddError(fmt.Sprintf("Disconnect client: %s", clients[connection].ID))
			delete(clients, connection)
			break
		}

		logs.AddPrimaryLog(fmt.Sprintf("Received from %s: %s", clients[connection].ID, receiveMessage))
		fmt.Printf("\n%#v\n", receiveMessage)

	}
}
